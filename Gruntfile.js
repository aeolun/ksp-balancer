module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    supervisor: {
      target: {
        script: "app.js"
      }
    },
    concurrent: {
      autoreload: {
        tasks: ['supervisor', 'watch']
      },
      options: {
        logConcurrentOutput: true
      }
    },
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: ['public/javascripts/src/**/*.js'],
        dest: 'public/javascripts/build/<%= pkg.name %>.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'public/javascripts/build/<%= pkg.name %>.js',
        dest: 'public/javascripts/build/<%= pkg.name %>.min.js'
      }
    },
    jshint: {
      files: ['Gruntfile.js', 'public/javascripts/src/**/*.js', 'test/**/*.js'],
      options: {
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
    watch: {
      files: ['public/javascripts/src/**/*.js'],
      tasks: ['concat', 'uglify'],
      options: {
        livereload: true
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-supervisor');
  grunt.loadNpmTasks('grunt-concurrent');
  // Default task(s).
  grunt.registerTask('default', [ 'jshint', 'concat', 'uglify','concurrent:autoreload']);

};
