/**
 * Created by Bart on 19/07/14.
 */
(function() {
  var app = angular.module('kspBalance', []);

  app.filter('orderByRecurse', function(){
    return function(input, attribute, direction) {
      //if (!angular.isObject(input)) return input;

      var keys = attribute.replace(/part\./,"").split('.');
      //console.log(keys, input);

      input.sort(function(a, b){
        for(var i = 0; i < keys.length; i++) {
          a = a[keys[i]];
        }
        for(var j = 0; j < keys.length; j++) {
          b = b[keys[j]];
        }
        if(typeof a === 'string') a = a.toLowerCase();
        if(typeof b === 'string') b = b.toLowerCase();
        if (a < b )
          return -1;
        if ( a > b )
          return 1;
        return 0;
      });
      return input;
    };
  });

  app.controller('PartList', ['$scope', '$http', function($scope, $http) {
    this.sortBy = 'name';
    this.sortDesc = false;
    this.parts = parts;
    this.tab = 'fields';
    this.locations = {};
    var stuff = this.locations;
    locs.forEach(function(item) {
      stuff[item.name] = item;
    });
    this.filter = null;
    this.toggleSelection = function(loc) {
      if(this.selectedLocations.indexOf(loc) > -1) {
        this.selectedLocations.splice(this.selectedLocations.indexOf(loc), 1);
      } else {
        this.selectedLocations.push(loc);
      }
    };
    this.setTab = function(tab) {
      this.tab = tab;
    };
    this.isTab = function(tab) {
      return this.tab == tab;
    };
    this.update = function(loc) {
      $http.post('location/update', { loc: loc.name, read: loc.readName }).success(function() {

      });
    };
    this.containsAllFields = function(part) {
      var missing = true;
      this.selectedLocations.forEach(function(item, index) {
        var keys = item.replace(/part\./,"").split('.');
        var a = part;
        for(var i = 0; i < keys.length; i++) {
          if (typeof a[keys[i]] === 'undefined' || a[keys[i]] === null || a[keys[i]] === '') {
            missing = false;
            return;
          }
          a = a[keys[i]];
        }

        if (typeof a === 'undefined' || a === null || a === '') {
          missing = false;
        }
      });
      return missing;
    };
    this.selectedLocations = [];
    this.sort = function(field) {
      if (field == this.sortBy) {
        this.sortDesc = !this.sortDesc;
      } else {
        this.sortBy = field;
        this.sortDesc = false;
      }
    };
    this.isSort = function(field) {
      return this.sortBy == field;
    };
  }]);
})();