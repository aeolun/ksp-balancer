var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var kue = require('kue'), jobs = kue.createQueue();
var fs = require('fs');
var find = require('findit');
var util = require('util');
var mongo = require('mongojs');

var u = require('underscore');


var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

var databaseUrl = "kspbalancer"; // "username:password@example.com/mydb"
var collections = ["parts", "locations"];
var db = mongo.connect(databaseUrl, collections);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

var parts = [];

jobs.process('reload parts', function(job, done, ctx){
  console.log('reload started');
  db.parts.remove({});

  var search = find(job.data.directory);
  var totalFiles = 0;
  var firstPart = null;
  search.on('file', function(file, stat) {
    if (file.slice(-8) == 'part.cfg') {
      totalFiles++;
      firstPart = file;
    }
  });
  search.on('error', function (err) {
    done(err);
  });
  search.on('end', function() {


    job.log('files to process: '+totalFiles);
    var search = find(job.data.directory);
    var filesRead = 0;
    search.on('file', function(file, stat) {
      if (file.slice(-8) == 'part.cfg') {
        fs.readFile(file, 'utf8', function (err,data) {
          var lines = data.split("\n");
          var category = "";
          var part = {};
          var parentPart = [];
          var level = 0;

          function setVar(part, linetext) {
            var stuff = linetext.split("=");
            var key = stuff[0].trim();
            var value = stuff[1].trim();
            if (isNaN(value)) {
              if (value.indexOf(',') != -1 && value.replace(/[a-z]+/, "") == value) {
                value = value.split(',');
                value = value.map(function(value, index, array) {
                  if(isNaN(value)) return value; else return parseFloat(value);
                });
              } else if (value.indexOf(' ') != -1 && value.replace(/[a-z]+/, "") == value) {
                value = value.split(' ');
                value = value.map(function(value, index, array) {
                  if(isNaN(value)) return value; else return parseFloat(value);
                });
              }
            } else {
              value = parseFloat(value);
            }

            if (Array.isArray(part[key]) && Array.isArray(part[key][0])) {
              part[key][part[key].length] = value;
            } else if (typeof part[key] !== 'undefined') {
              part[key] = [ part[key] ];
              part[key][part[key].length] = value;
            } else {
              part[key] = value;
            }
          }

          for(var line in lines) {
            var linetext = lines[line].trim();
            if (linetext.substr(0,2) == '//') continue;
            if (linetext.indexOf('{') != -1 && linetext.indexOf('}') != -1 && linetext.indexOf('=') != -1) {
              //console.log('start');
              var stuff = linetext.split('{');
              category = stuff[0].trim();

              parentPart[level] = part;
              if (Array.isArray(part[category])) {
                part[category][part[category].length] = {};
                part = part[category][part[category].length-1];
              } else if (typeof part[category] !== 'undefined') {
                part[category] = [ part[category] ];
                part[category][part[category].length] = {};
                part = part[category][part[category].length-1];
              } else {
                part[category] = {};
                part = part[category];
              }
              level++;

              setVar(part, stuff[1].replace(/\}/, '').trim());

              //console.log('end');
              part = parentPart[level-1];
              level--;

            } else if (linetext.indexOf('{') != -1 && linetext.length > 1) {
              //console.log('named start');
              category = linetext.replace(/\{/, "").trim();
              parentPart[level] = part;
              if (Array.isArray(part[category])) {
                part[category][part[category].length] = {};
                part = part[category][part[category].length-1];
              } else if (typeof part[category] !== 'undefined') {
                part[category] = [ part[category] ];
                part[category][part[category].length] = {};
                part = part[category][part[category].length-1];
              } else {
                part[category] = {};
                part = part[category];
              }
              level++;
            } else if (linetext.indexOf('{') != -1) {
              //console.log('start');
              parentPart[level] = part;
              if (Array.isArray(part[category])) {
                part[category][part[category].length] = {};
                part = part[category][part[category].length-1];
              } else if (typeof part[category] !== 'undefined') {
                part[category] = [ part[category] ];
                part[category][part[category].length] = {};
                part = part[category][part[category].length-1];
              } else {
                part[category] = {};
                part = part[category];
              }
              level++;
            } else if (linetext.indexOf('}') != -1) {
              //console.log('end');
              part = parentPart[level-1];
              level--;
            } else if (linetext.indexOf('=') != -1) {
              //console.log('assignment');
              setVar(part, linetext);
            } else {
              //console.log('name category');
              category = linetext.trim();
            }
          }
          function replaceItems(root) {
            for(var key in root) {
              if (Array.isArray(root[key]) && typeof root[key][0].name !== 'undefined') {
                newObject = {};
                for(var subkey in root[key]) {
                  var mod = root[key][subkey];

                  newObject[mod.name] = mod;
                  delete newObject[mod.name]['name'];
                }
                newObject = replaceItems(newObject);
                root[key] = newObject;
              }
            }
            return root;
          }
          parts = [];
          if (!Array.isArray(parentPart[0]['PART'])) {
            parts.push(parentPart[0]['PART']);
          } else {
            parts = parentPart[0]['PART'];
          }
          for(var i = 0; i < parts.length; i++) {
            parts[i] = replaceItems(parts[i]);
          }

          for(var i = 0; i < parts.length; i++) {
            db.parts.save(parts[i], function(err, saved) {
              if( err || !saved ) done(err);
              else console.log("Part saved");
            });
          }
          filesRead++;
          job.progress(filesRead, totalFiles);
        });
      }
    });
    search.on('end', function() {
      done(null, 'reloaded '+filesRead+' files');
    });
    search.on('error', function (err) {
      done(err);
    });
  });
});


module.exports = app;

app.listen(3000);
kue.app.listen(3001);