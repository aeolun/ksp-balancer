var express = require('express');
var router = express.Router();
var kue = require('kue'), jobs = kue.createQueue();

var databaseUrl = "kspbalancer"; // "username:password@example.com/mydb"
var collections = ["parts", "locations"];
var db = require('mongojs').connect(databaseUrl, collections);

var u = require('underscore');

/* GET home page. */
router.get('/', function(req, res) {
  db.parts.find({}, {}, function(e,docs){
    db.locations.find({}, {}, function(e,locdocs) {
      res.render('index', { parts: JSON.stringify(docs), locs: JSON.stringify(locdocs) });
    })
  });
});

router.post('/location/update', function(req, res) {
  db.locations.update(
    { name: req.param('loc') },
    {
      $set: { readName: req.param('read') }
    },
    function(err, count) {
      console.log(err, count);
    }
  );
  res.send(200, 'OK');
});

router.get('/update', function(req, res) {
  var job = jobs.create('reload parts', {
    directory: 'GameData/'
  }).priority('high').save();

  job.on('complete', function(result){
    console.log("Job completed with data ", result);
    db.parts.find({}, {}, function(e,docs){
      var listNodes = function(doc, parent) {
        if (typeof doc === 'object') {
          var stuff = [];
          Object.keys(doc).forEach(function(index) {
            var value = doc[index];

            if(index != '_id') {
              var sub = listNodes(value, parent+'.'+index);
            }
            if (typeof sub === 'object') {
              for(var k in sub) {
                stuff.push(sub[k]);
              }
            } else {
              stuff.push(sub);
            }
          });
          return stuff;
        } else {
          return parent;
        }
      }
      var locs = [];
      for(var i = 0; i < docs.length; i++) {
        locs = u.union(locs, listNodes(docs[i], 'part'));
      }
      locs.forEach(function(item) {
        db.locations.findAndModify({
          query: { name: item },
          update: {
            $setOnInsert: { name: item }
          },
          new: true,
          upsert: true
        });
      });
      console.log(locs.length+' locations inserted')
    });
  }).on('failed', function(){
    console.log("Job failed");
  }).on('progress', function(progress){
    //console.log('job #' + job.id + ' ' + progress + '% complete');
  });

  res.send(200, "Update Initialized");
})

module.exports = router;
